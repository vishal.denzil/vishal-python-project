# from matplotlib import pyplot as plot
import mainfile
def extra_runs_consider_by_year(year):
    #content=mainfile.open_file_matches()#returns splitted lines
    align=mainfile.open_file_matches() # splitting based on new line
    id=list()
    for lines in range (1,len(align)-1):#reading line by line
        line=align[lines].split(',')#splitting each line
        if line[1] == str(year):#extracting the only 2016 data from file
            id.append(int(line[0]))

    list_of_ids(id)

def list_of_ids(id):
    #data=mainfile.open_file_delivers()
    make=mainfile.open_file_delivers()
    dictionary={}
    for var in range (1,len(make)-1):
        line = make[var].split(',')
        dictionary[line[3]]=0

    for var in range (1,len(make)-1):
        line=make[var].split(',')
        value=(int(line[0]))
        if value in id:
            #print(line[3])
            dictionary[line[3]] += int(line[16])
    plot_bar_chart(dictionary)
def plot_bar_chart(dictionary):
    key=[]
    value=[]
    for keys in sorted(dictionary.keys()):
        if dictionary[keys]>0:
            key.append(keys)
            value.append(dictionary[keys])

    mataData= {
        'x' :"Teams",
        'y' :"Extras",
        'title' :"Number Of Extra Runs in 2016"
    }
    mainfile.plot_barchat_show(key,value,mataData)

extra_runs_consider_by_year(2016)