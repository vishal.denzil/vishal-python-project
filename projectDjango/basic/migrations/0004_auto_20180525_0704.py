# Generated by Django 2.0.5 on 2018-05-25 07:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basic', '0003_auto_20180525_0703'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivers',
            name='fielder',
            field=models.CharField(max_length=240),
        ),
    ]
