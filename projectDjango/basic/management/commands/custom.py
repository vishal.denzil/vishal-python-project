from django.core.management.base import BaseCommand, CommandError
from projectDjango.database import database_logic,insertion
from django.db import transaction
class Command(BaseCommand):
    help = 'Closes the specified poll for voting'
    def handle(self, *args, **options):
            insertion.insert_match()
            insertion.insert_deliver()
        