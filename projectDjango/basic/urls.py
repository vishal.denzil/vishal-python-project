from django.conf.urls import url
from . import views

urlpatterns = [
    url('first/',views.number_of_match,name='index'),
    url('third/',views.number_of_extra_runs,name='index'),
    url('fourth/',views.economic_bowler,name='index'),
    url('second/',views.number_matches_per_year,name='index'),
    url('story/',views.toss_winner,name='index'),
]