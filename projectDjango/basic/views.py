
from django.http import HttpResponse
from django.db import transaction
from projectDjango.database import database_logic
from .models import matches,delivers
import csv 
# from django.template import loader
from django.shortcuts import render

# Create your views here.

def number_of_match(request):
    x,y=database_logic.number_of_matches_per_year()
    content={
        'text' :"Matches Played Per Year",
        'name':"Year List"
       }
    chart=database_logic.plot_chart(x,y,content)
    return render(request,'basic/index.html',context={'chart':chart})


def number_of_extra_runs(request):
    x,y=database_logic.number_of_extra_runs_considered_by_team(2016)
    content={
        'text' :"Number Of Extra Runs In 2016",
        'name':"Team List"
       }
    chart=database_logic.plot_chart(x,y,content)
    return render(request,'basic/index.html',context={'chart':chart})


def economic_bowler(request):
    x,y=database_logic.economic_bowler_per_year(2016)    
    content={
        'text' :"Economic Bowler of 2016",
        'name':"Bowlers List"
       }
    chart=database_logic.plot_chart(x[:10],y[:10],content)
    return render(request,'basic/index.html',context={'chart':chart})


def number_matches_per_year(request):
    x,y=database_logic.number_of_winners_in_each_year()
    values_data=list()
    color_values=0
    get_color = ['blue', 'green', 'orange', 'black', 'purple', 'pink', 'darkgray', 'crimson', 'blueviolet', 'indigo', 'khaki',
         'brown', 'lawngreen', 'teal']
    for key in y.keys():
        values_data.append({'color':get_color[color_values], 'name':key, 'data':y[key]})
        color_values+=1
    content={
        'text' :"Number Of Winners Per Year",
        'name':"Team List"
       }
    chart=database_logic.stack_chart(x,y,values_data,content)
    return render(request,'basic/index.html',context={'chart':chart})

def toss_winner(request):
    x,y=database_logic.number_of_toss_winner_in_each_year()
    values_data=list()
    color_values=0
    get_color = ['blue', 'green', 'orange', 'black', 'purple', 'pink', 'darkgray', 'crimson', 'blueviolet', 'indigo', 'khaki',
         'brown', 'lawngreen', 'teal']
    for key in y.keys():
        values_data.append({'color':get_color[color_values], 'name':key, 'data':y[key]})
        color_values+=1
    content={
        'text' :"Number Of Toss Winners Per Year",
        'name':"Team List"
       }
    chart=database_logic.stack_chart(x,y,values_data,content)
    return render(request,'basic/index.html',context={'chart':chart})

    # return HttpResponse("<h1> done</h1>")
# def openfile():
#     with open('matches.csv','r') as csvfile:
#         csvread = csv.DictReader(csvfile)
        
#         filelist=[]
#         for row in csvread:
#              filelist.append(dict(row))

#     for data in filelist:
    
#         match_data=matches(
#         id=data['id'],  
#         season=data['season'],
#         city=data['city'],
#         date=data['date'],
#         team1=data['team1'],
#         team2=data['team2'],
#         toss_winner=data['toss_winner'],
#         toss_decision=data['toss_decision'],
#         result=data['result'],
#         dl_applied=data['dl_applied'],
#         winner=data['winner'],
#         win_by_runs=data['win_by_runs'],
#         win_by_wickets=data['win_by_wickets'],
#         player_of_match=data['player_of_match'],
#         venue=data['venue'],
#         umpire1=data['umpire1'],
#         umpire2=data['umpire2'],
#         )
#         match_data.save()
# def insert_deliver():
#     with open('deliveries.csv','r') as csvfile:
#         csvread = csv.DictReader(csvfile)
        
#         filelist=[]
#         for row in csvread:
#             filelist.append(dict(row))
        
#         for data in filelist:
#             deliver=delivers(
#             match_id=matches.objects.get(pk=data['match_id']),
#             inning=data['inning'],
#             batting_team=data['batting_team'],
#             bowling_team=data['bowling_team'],
#             over=data['over'],
#             ball=data['ball'],
#             batsman=data['batsman'],
#             non_striker=data['non_striker'],
#             bowler=data['bowler'],
#             is_super_over=data['is_super_over'],
#             wide_runs=data['wide_runs'],
#             bye_runs=data['bye_runs'],
#             legbye_runs=data['legbye_runs'],
#             noball_runs=data['noball_runs'],
#             penalty_runs=data['penalty_runs'],
#             batsman_runs=data['batsman_runs'],
#             extra_runs=data['extra_runs'],
#             total_runs=data['total_runs'],
#             player_dismissed=data['player_dismissed'],
#             dismissal_kind=data['dismissal_kind'],
#             fielder=data['fielder'])
#             deliver.save()

        