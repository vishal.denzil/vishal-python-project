from django.db import models

# Create your models here.
class matches(models.Model):
    id=models.IntegerField(primary_key=True)
    season=models.IntegerField()
    city=models.CharField(max_length=230)
    date=models.DateField(max_length=100)
    team1=models.CharField(max_length=250)
    team2=models.CharField(max_length=250)
    toss_winner=models.CharField(max_length=250)
    toss_decision=models.CharField(max_length=250)
    result=models.CharField(max_length=250)
    dl_applied=models.IntegerField()
    winner=models.CharField(max_length=250)
    win_by_runs=models.IntegerField()
    win_by_wickets=models.IntegerField()
    player_of_match=models.CharField(max_length=250)
    venue=models.CharField(max_length=250)
    umpire1=models.CharField(max_length=250)
    umpire2=models.CharField(max_length=250)
class delivers(models.Model):
    match_id=models.ForeignKey(matches,on_delete=models.CASCADE)
    inning=models.IntegerField()
    batting_team=models.CharField(max_length=230)
    bowling_team=models.CharField(max_length=230)
    over=models.IntegerField()
    ball=models.IntegerField()
    batsman=models.CharField(max_length=240)
    non_striker=models.CharField(max_length=245)
    bowler=models.CharField(max_length=240)
    is_super_over=models.IntegerField()
    wide_runs=models.IntegerField()
    bye_runs=models.IntegerField()
    legbye_runs=models.IntegerField()
    noball_runs=models.IntegerField()
    penalty_runs=models.IntegerField()
    batsman_runs=models.IntegerField()
    extra_runs=models.IntegerField()
    total_runs=models.IntegerField()
    player_dismissed=models.CharField(max_length=240)
    dismissal_kind=models.CharField(max_length=240)
    fielder=models.CharField(max_length=250)





