from django.http import HttpResponse
from basic.models import matches,delivers
from django.db.models import Count,Sum,Q
from copy import deepcopy
def number_of_matches_per_year():
   matches_per_year_dict={}
   match_data=matches.objects.values('season').annotate(number_of_matches=Count('season'))
   x=[]
   y=[]
   for match in match_data:
       x.append(match['season'])
       y.append(match['number_of_matches'])
   return x,y

def number_of_extra_runs_considered_by_team(year):
    year_wise_id=matches.objects.all().filter(season=2016).values_list('id',flat=True)
    deliver_data=delivers.objects.filter(match_id__lte=max(year_wise_id),match_id__gte=min(year_wise_id))\
    .values('bowling_team').annotate(sum=Sum('extra_runs'))
    x=[]
    y=[]
    for extra_run in deliver_data:
        x.append(extra_run['bowling_team'])
        y.append(extra_run['sum'])
    return x,y
    # extra_runs_of_team={}
    # for i in deliver_data:
    #     extra_runs_of_team[i['bowling_team']]=i['sum']
    # print(extra_runs_of_team)

def number_of_winners_in_each_year():
    year_match_count={}
    team_wise={}
    team_winner_count=matches.objects.filter(~Q(winner="")).values('season','winner').annotate(count=Count('winner')).order_by('season')
    print(team_winner_count)
    for year_team in team_winner_count:
        team_wise[year_team['winner']]=0
    for year_team in team_winner_count:
        year_match_count[year_team['season']]=deepcopy(team_wise)
    print(year_match_count)
    for year_team in team_winner_count:
        # team_wise[year_team['winner']]=year_team['count']
        # year_match_count[year_team['season']]=deepcopy(team_wise)
        year_match_count[year_team['season']][year_team['winner']]=year_team['count']
   
    x=[]
    y={}
    for year_match in sorted(year_match_count):
        x.append(year_match)
        for team_wise in sorted(year_match_count[year_match]):
            if team_wise in y:
                y[team_wise].append(year_match_count[year_match][team_wise])
            else:
                y[team_wise]=[year_match_count[year_match][team_wise]]
    return x,y
                
    # for year_match in year_match_count.keys():
    #     for team in year_match_count[year_match].keys():
    #         x1.append(year_match_count[year_match][team])
    #     x.append(x1)
    # x=[*zip(*x)]
    # print(x)

    
def economic_bowler_per_year(year):
    economy_bowler={}
    bowler_total_runs_by_bowl=delivers.objects.filter(match_id__season=year).values('bowler').\
    annotate(total_bowl=Count('bowler'),score=Sum('total_runs'))
    #print(bowler_total_runs_by_bowl)
    x=[]
    y=[]
    for run_bowl in bowler_total_runs_by_bowl:
        economy_bowler[run_bowl['bowler']]=(run_bowl['score'])/(run_bowl['total_bowl'])
        # x.append(run_bowl['bowler'])
        # y.append((run_bowl['score'])/(run_bowl['total_bowl']))

    for economy in sorted(economy_bowler.keys(),key=economy_bowler.get):
        x.append(economy)
        y.append(economy_bowler[economy])
    return x,y


def number_of_toss_winner_in_each_year():
    year_toss_winner={}
    team_wise={}
    team_winner_count=matches.objects.filter(~Q(toss_winner="")).values('season','toss_winner').\
    annotate(count=Count('toss_winner')).order_by('season')
    for year_team in team_winner_count:
        team_wise[year_team['toss_winner']]=0
    for year_team in team_winner_count:
        year_toss_winner[year_team['season']]=deepcopy(team_wise)
    for year_team in team_winner_count:
        # team_wise[year_team['winner']]=year_team['count']
        # year_match_count[year_team['season']]=deepcopy(team_wise)
        year_toss_winner[year_team['season']][year_team['toss_winner']]=year_team['count']
   
    x=[]
    y={}
    for year_match in sorted(year_toss_winner):
        x.append(year_match)
        for team_wise in sorted(year_toss_winner[year_match]):
            if team_wise in y:
                y[team_wise].append(year_toss_winner[year_match][team_wise])
            else:
                y[team_wise]=[year_toss_winner[year_match][team_wise]]
    return x,y    
def plot_chart(x,y,content):
    chart  = {
      'chart': {'type': 'column'},
      'title': {'text': content['text']},
      'xAxis':{'categories': x,'name':content['name']},
      'series':[{'name':content['name'],'data':y}]
        }
    return chart
def stack_chart(x,y,values_data,content):
    chart  = {
      'chart': {'type': 'column'},
      'title': {'text': content['text']},
      'plotOptions':{'series':{'stacking':'normal'}},
      'xAxis':{'categories': x,'name':content['name'] },
      'series':values_data
        }
    return chart