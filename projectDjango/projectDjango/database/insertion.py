from basic.models import matches,delivers
import csv 
from django.db  import transaction

@transaction.atomic
def insert_match():
    with open('matches.csv','r') as csvfile:
        csvread = csv.DictReader(csvfile)
        
        filelist=[]
        for row in csvread:
             filelist.append(dict(row))

    for data in filelist:
    
        match_data=matches(
        id=data['id'],  
        season=data['season'],
        city=data['city'],
        date=data['date'],
        team1=data['team1'],
        team2=data['team2'],
        toss_winner=data['toss_winner'],
        toss_decision=data['toss_decision'],
        result=data['result'],
        dl_applied=data['dl_applied'],
        winner=data['winner'],
        win_by_runs=data['win_by_runs'],
        win_by_wickets=data['win_by_wickets'],
        player_of_match=data['player_of_match'],
        venue=data['venue'],
        umpire1=data['umpire1'],
        umpire2=data['umpire2'],
        )
        match_data.save()
@transaction.atomic
def insert_deliver():
    with open('deliveries.csv','r') as csvfile:
        csvread = csv.DictReader(csvfile)
        
        filelist=[]
        for row in csvread:
            filelist.append(dict(row))
        
        for data in filelist:
            deliver=delivers(
            match_id=matches.objects.get(pk=data['match_id']),
            inning=data['inning'],
            batting_team=data['batting_team'],
            bowling_team=data['bowling_team'],
            over=data['over'],
            ball=data['ball'],
            batsman=data['batsman'],
            non_striker=data['non_striker'],
            bowler=data['bowler'],
            is_super_over=data['is_super_over'],
            wide_runs=data['wide_runs'],
            bye_runs=data['bye_runs'],
            legbye_runs=data['legbye_runs'],
            noball_runs=data['noball_runs'],
            penalty_runs=data['penalty_runs'],
            batsman_runs=data['batsman_runs'],
            extra_runs=data['extra_runs'],
            total_runs=data['total_runs'],
            player_dismissed=data['player_dismissed'],
            dismissal_kind=data['dismissal_kind'],
            fielder=data['fielder'])
            deliver.save()
