from matplotlib import pyplot as plot
from copy import deepcopy
import mainfile
def year_wise_team_winning():
    align=mainfile.open_file_matches() # splitting based on new line
    year=list()
    winner=list()
    for lines in range (1,len(align)-1):#reading line by line
        line=align[lines].split(',')#splitting each line
        year.append(int(line[1]))#takin year
        winner.append(line[10])
    passing_year_winner(year,winner,align)#passing winner and year list and align
def  passing_year_winner(year,winner,align):
    dict={}
    document={}
    for val in winner:
        win=list(filter(lambda x:x!=" ",val))
        if win:
            dict[val] = 0#initializing
    for var in year:
        document[var]=deepcopy(dict);#deepcopy used for doesnot effect the any other copy
        #print(document)

    for lines in range (1,len(align)-1):#reading line by line
        line=align[lines].split(',')#splitting each line
        year=int(line[1])#takin year
        winner=line[10]
        win = list(filter(lambda x: x != " ",winner))
        if win:
            document[year][winner]+=1
    plot_the_stack_bar_chat(document,dict)
def  plot_the_stack_bar_chat(document,dict):

    plot.rcParams['figure.figsize']=(20,18)
    margin_value={
        "left":0.04,
        "bottom":0.08,
        "right":0.8,
        "top":0.95,
        }
    plot.subplots_adjust(**margin_value)
    c = ['blue', 'green', 'orange', 'black', 'purple', 'pink', 'darkgray', 'crimson', 'blueviolet', 'indigo', 'khaki',
         'brown', 'lawngreen', 'teal']
    for season in sorted(document):
        y_pos=0
        i=0
        for team in sorted(document[season]):
            plot.bar(season,document[season][team],bottom=y_pos,color=c[i])
            y_pos+=document[season][team]
            i+=1
    plot.xticks(sorted(document))
    #plot.show()

    teamsdict=list(dict.keys())
    print(teamsdict)
    plot.xlabel('years')
    plot.ylabel('Matches played in year')
    plot.title('Winners of the season per year')
    plot.legend(sorted(teamsdict),framealpha=0.6,loc=2,bbox_to_anchor=(1,1))
    plot.show()

year_wise_team_winning()