import mainfile
def toss_winner_in_2015():
    #content=mainfile.open_file_matches()#returns splitted lines
    align=mainfile.open_file_matches() # splitting based on new line
    toss={}
    for lines in range (1,len(align)-1):#reading line by line
        line=align[lines].split(',')#splitting each line
        if line[1] == '2015':#extracting the only 2016 data from file
          toss[line[6]]=0
    for lines in range (1,len(align)-1):#reading line by line
        line=align[lines].split(',')#splitting each line
        if line[1] == '2015':#extracting the only 2016 data from file
          toss[line[6]]+=1
    plot_bar_chart_2015(toss)
def plot_bar_chart_2015(toss):
    key = []
    value = []
    for var in sorted(toss):
        key.append(var)
        value.append(toss[var])
    x = "number of teams"
    y = "Number of toss"
    title = "toss winner in 2015"
    mainfile.plot_barchat_show(key, value, x, y, title)
toss_winner_in_2015()