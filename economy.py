from matplotlib import pyplot as plot
from copy import deepcopy
import mainfile

file = mainfile.open_file_delivers()  # splitting based on new line
def economy_of_bowler(year):
    align = mainfile.open_file_matches()  # splitting based on new line
    id = list()
    for lines in range(1, len(align) - 1):  # reading line by line
        line = align[lines].split(',')  # splitting each line
        if line[1] == str(year):
            id.append(int(line[0]))
    passing_2015_id(id)
def passing_2015_id(id):
    #print(id)
    bowler_eco={}
    bowler_score={}
    bowler_bowl={}
    bowler={}
    for lines in range(1,len(file)-1):#reading line by line
        line=file[lines].split(',')#splitting lines
        #print(id)
        if int(line[0]) in id:
            bowler_eco[line[8]]=0.0#assigning economy
            bowler_score[line[8]]=0 #assign scores
            bowler_bowl[line[8]]=0#assingn bowls
            bowler[line[8]]=0
    passing_economy_score_bowl(bowler_eco,bowler_score,bowler_bowl,id)
    #print(bowlerscore)
def  passing_economy_score_bowl(bowler_eco,bowler_score,bowler_bowl,id):
    score=list()#to store the score
    bowl=0
    for lines in range(1,len(file)-1):
        line=file[lines].split(',')
        if int(line[0]) in id:
            bowler_score[line[8]]+=int(line[17])
            bowler_bowl[line[8]]+=1
    for key in bowler_eco.keys():
        bowler_eco[key] = bowler_score[key] / bowler_bowl[key]
    sorting_based_on_values(bowler_eco)
def sorting_based_on_values(bowler_eco):
    key=[]
    value=[]
    sorteditem = sorted(bowler_eco.items(), key=lambda t:t[1])#sorting based on values in dictionary so t[1]
    for keys in sorteditem:
        key.append(keys[0])#key
        value.append(keys[1])#values
    metaData = {
        'x' : "best 10 economy bowler",
        'y' : "economy rate",
        'title' : "2015 economy bowler",
    }

    mainfile.plot_barchat_show(key[:10], value[:10],metaData)#best the ten aconomies am taking



economy_of_bowler(2015)