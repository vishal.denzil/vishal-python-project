from matplotlib import pyplot as plot


def open_file_matches():
    fm = open("/home/dev/python/ipl/matches.csv", "r")
    content=fm.read()
    contentline=content.split('\n')
    return contentline
def open_file_delivers():
    fd = open("/home/dev/python/ipl/deliveries.csv", "r")
    content=fd.read()
    contentline = content.split('\n')
    return contentline
def plot_barchat_show(key, value,mataData):
    print(key,value)
    plot.bar(key, value, color="blue", tick_label=key)
    plot.xticks(key, rotation='vertical')
    plot.xlabel(mataData['x'],fontsize=12)
    plot.ylabel(mataData['y'])
    plot.title(mataData['title'])
    plot.show()